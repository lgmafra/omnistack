import React from 'react';
import { YellowBox } from 'react-native'

YellowBox.ignoreWarnings(["Unreco"])

import Routes from './routes';

const App = () => <Routes />;

export default App;
